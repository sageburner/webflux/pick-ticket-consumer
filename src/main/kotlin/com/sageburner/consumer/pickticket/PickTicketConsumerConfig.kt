package com.sageburner.consumer.pickticket

import com.sageburner.consumer.pickticket.model.PickTicketEvent
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.kafka.support.LoggingProducerListener
import org.springframework.kafka.support.ProducerListener
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.util.ObjectUtils
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import java.text.SimpleDateFormat

@Configuration
@EnableTransactionManagement
class PickTicketConsumerConfig {

    /**
     * Kafka Configuration
     */

    @Autowired
    private lateinit var env: Environment

    val dateFormat = SimpleDateFormat("HH:mm:ss:SSS z dd MMM yyyy")

    /**
     * Reactor Kafka Configuration
     */

    @Bean
    fun kafkaSender(): KafkaSender<String, String> {

        val props: MutableMap<String, Any> = mutableMapOf()
        env.getProperty("kafka.bootstrap.servers")?.let {
            props[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = it
        }
        props[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        props[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java

        // first attempt at Kafka transactions
        // enable 'exactly once' semantics
//        props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true)

        // set 'TransactionId', enable transactions
//        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "myTX")

        // configure transaction timeout
//        props.put(ProducerConfig.TRANSACTION_TIMEOUT_CONFIG, 3000)

        props[ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION] = 1
        props[ProducerConfig.ACKS_CONFIG] = "1"
        val senderOptions = SenderOptions.create<String, String>(props)

        return KafkaSender.create(senderOptions)
    }

    /**
     * Spring Kafka Configuration
     */

    @Bean
    fun kafkaTemplate(): KafkaTemplate<String, PickTicketEvent> {
        val kafkaTemplate = KafkaTemplate(producerFactory())
        kafkaTemplate.setProducerListener(producerListener())
        return kafkaTemplate
    }

    @Bean
    fun producerFactory(): ProducerFactory<String, PickTicketEvent> {
        val pf: DefaultKafkaProducerFactory<String, PickTicketEvent> = DefaultKafkaProducerFactory(producerConfigs())
        // set 'TransactionId', enable transactions
        pf.setTransactionIdPrefix("myTX.")
        return pf
    }

    @Bean
    fun producerConfigs(): Map<String, Any> {
        val props: MutableMap<String, Any> = mutableMapOf()
        env.getProperty("kafka.bootstrap.servers")?.let {
            props[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = it
        }
        props[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        props[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java

        // first attempt at Kafka transactions
        // enable 'exactly once' semantics
        props[ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG] = true

        // set 'TransactionId', enable transactions
//        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "myTX")
        // configure transaction timeout
        props[ProducerConfig.TRANSACTION_TIMEOUT_CONFIG] = 3000

        props[ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION] = 1

        return props
    }

    @Bean
    fun producerListener(): ProducerListener<String, PickTicketEvent> {
        return MyLoggingProducerListener<String, PickTicketEvent>()
    }
}

class MyLoggingProducerListener<K, V> : LoggingProducerListener<K, V>() {
    private val log = LoggerFactory.getLogger(MyLoggingProducerListener::class.java)

//    override fun isInterestedInSuccess(): Boolean {
//        return true
//    }


    override fun onSuccess(producerRecord: ProducerRecord<K, V>, recordMetadata: RecordMetadata?) {
        val logOutput = StringBuffer()
        logOutput.append("Message sent")
        logOutput.append(" with key='"
                + toDisplayString(ObjectUtils.nullSafeToString(producerRecord.key()), 100) + "'")
        logOutput.append(" and payload='"
                + toDisplayString(ObjectUtils.nullSafeToString(producerRecord.value()), 100) + "'")
        logOutput.append(" to topic ${producerRecord.topic()}")
        if (producerRecord.partition() != null) {
            logOutput.append(" and partition ${producerRecord.partition()}")
        }
        if (recordMetadata != null) {
            logOutput.append(" with metadata $recordMetadata")
        }
        logOutput.append(":")
        log.info(logOutput.toString())
    }

    private fun toDisplayString(original: String, maxCharacters: Int): String {
        return if (original.length <= maxCharacters) {
            original
        } else original.substring(0, maxCharacters) + "..."
    }
}
