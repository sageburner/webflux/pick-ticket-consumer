package com.sageburner.consumer.pickticket.model

import java.util.*

/**
 * {
 *   "supplyingLocation": {
 *     "locationId": "1200047ad9b47b0000000b9150e941",
 *     "locationName": "Supplying Location"
 *   },
 *   "requestingLocation": {
 *     "locationId": "00000dd8c3ecf40000000b9f2c5d0b",
 *     "locationName": "ER"
 *   },
 *   "note": "who let the dogs out?",
 *   "caseNumber": "",
 *   "requisitionLines": [
 *     {
 *       "qtyNeeded": "10",
 *       "parId": "120008998d213a0000000b0b37455d"
 *     }
 *   ]
 * }
 */
data class PickTicket(
        val id: UUID,
        val supplyingLocation: Location,
        val requestingLocation: Location,
        val note: String = "note",
        val caseNumber: String?,
        val requisitionLines: List<RequisitionLine>)
