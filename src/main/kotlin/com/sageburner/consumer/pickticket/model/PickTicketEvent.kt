package com.sageburner.consumer.pickticket.model

import java.time.OffsetDateTime
import java.util.*

data class PickTicketEvent(
        val id: UUID = UUID.randomUUID(),
        val type: PickTicketEventType,
        val timestamp: String = OffsetDateTime.now().toString(),
        val pickTicket: PickTicket)

enum class PickTicketEventType {
    CREATED, MODIFIED
}
