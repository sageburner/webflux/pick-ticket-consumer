package com.sageburner.consumer.pickticket.repository.kafka

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.sageburner.consumer.pickticket.model.PickTicketEvent
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import java.util.*

/**
 * PickTicketKafkaRepository
 *
 */
@Component
class PickTicketKafkaRepository {
    private val log = LoggerFactory.getLogger(PickTicketKafkaRepository::class.java)

    @Autowired
    private lateinit var kafkaSender: KafkaSender<String, String>

    private val mapper = jacksonObjectMapper()

    fun sendRx(topic: String, payload: PickTicketEvent) {

        val mono : Mono<SenderRecord<String, String, String>> = Mono.just(
                SenderRecord.create(ProducerRecord<String, String>(topic, payload.pickTicket.id.toString(), jsonify(payload)), UUID.randomUUID().toString()))

        kafkaSender.send(mono)
                .doOnNext {
                    r -> log.info(String.format("Message #%s send response: %s\n", r.correlationMetadata(), r.recordMetadata()))
                }
                .doOnError { e -> log.error("Send failed", e) }
                .subscribe()
    }

    fun sendRxTransactionally(topic: String, payload: PickTicketEvent) {

        val flux : Flux<PickTicketEvent> = Flux.just(payload).doOnNext {
            r -> log.info(String.format("Message sent: %s", jsonify(r)))
        }

        fun record(data: PickTicketEvent) : Flux<SenderRecord<String, String, String>> {
            return Flux.just(SenderRecord.create(ProducerRecord<String, String>(topic, data.pickTicket.id.toString(), jsonify(data)), UUID.randomUUID().toString()))
        }

        kafkaSender.sendTransactionally(flux.map{ x -> record(x) })
                .doOnError { e -> log.error("Send failed", e) }
                .subscribe()
    }

    @Autowired
    private lateinit var kafkaTemplate: KafkaTemplate<String, PickTicketEvent>

    fun send(topic: String, payload: PickTicketEvent) {
        kafkaTemplate.send(topic, payload.id.toString(), payload)
    }

    fun sendTransactionally(topic: String, payload: PickTicketEvent) {
        kafkaTemplate.executeInTransaction { tx ->
            {
                tx.send(topic, payload.id.toString(), payload).exceptionally { e ->
                    log.error("OMG I HAS HAD FAILURE!!!")
                    null
                }
            }
        }
    }
    
    /*
    (object : ListenableFutureCallback<SendResult<String, PickTicketEvent>> {
                    override fun onSuccess(result: SendResult<String, PickTicketEvent>?) {
                        log.info("OMG I COMMITED SOMETHING!")
                    }

                    override fun onFailure(ex: Throwable) {
                        log.error("OMG I HAS HAD FAILURE!!!")
                    }
                })
     */

    private fun jsonify(input: Any): String {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(input)
    }

}