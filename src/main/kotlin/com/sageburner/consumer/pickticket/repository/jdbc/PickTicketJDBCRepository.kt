package com.sageburner.consumer.pickticket.repository.jdbc

import com.sageburner.consumer.pickticket.model.Location
import com.sageburner.consumer.pickticket.model.PickTicket
import com.sageburner.consumer.pickticket.model.RequisitionLine
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.util.*

/**
 * PickTicketJDBCRepository
 *
 * TODO: Implement Liquibase or Flyway
 * TODO: Implement retry/recovery strategy
 * TODO: Implement Ignite cache
 */
@Repository
class PickTicketJDBCRepository : InitializingBean, DisposableBean {
    private val log = LoggerFactory.getLogger(PickTicketJDBCRepository::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    private lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    override fun afterPropertiesSet() {
        log.info("Initializing...")
        createTables()
    }

    override fun destroy() {
        log.info("Destroying ...")
    }

    fun create(pickTicket: PickTicket) : PickTicket {
        val pickTicketId: UUID = insert(pickTicket)
        insertRequisitionLines(pickTicketId, pickTicket.requisitionLines)

        return pickTicket
    }

    fun insert(pickTicket: PickTicket): UUID {
        val id = pickTicket.id.toString()
        val supplyingLocationId = pickTicket.supplyingLocation.id.toString()
        val supplyingLocationName = pickTicket.supplyingLocation.name
        val requestingLocationId = pickTicket.requestingLocation.id.toString()
        val requestingLocationName = pickTicket.requestingLocation.name
        val note = pickTicket.note
        val caseNumber = pickTicket.caseNumber

        jdbcTemplate.update("INSERT INTO " +
                "pick_ticket(" +
                    "id," +
                    "supplyingLocationId, " +
                    "supplyingLocationName, " +
                    "requestingLocationId, " +
                    "requestingLocationName, " +
                    "note, " +
                    "caseNumber" +
                ") VALUES (?,?,?,?,?,?,?)",
                    id,
                    supplyingLocationId,
                    supplyingLocationName,
                    requestingLocationId,
                    requestingLocationName,
                    note,
                    caseNumber
        )

        return pickTicket.id
    }

    fun insertRequisitionLines(pickTicketId: UUID, requisitionLines: List<RequisitionLine>) {
        jdbcTemplate.batchUpdate("INSERT INTO requisition_line(id, pickTicketId, qtyNeeded, parId) VALUES (?,?,?,?)",
                requisitionLines.map { requisitionLine -> requisitionLinesToStringArray(pickTicketId, requisitionLine) })
    }

    fun requisitionLinesToStringArray(pickTicketId: UUID, requisitionLine: RequisitionLine): Array<String> {
        return listOf(requisitionLine.id.toString(), pickTicketId.toString(), requisitionLine.quantityNeeded, requisitionLine.parId.toString()).toTypedArray()
    }

    fun listAll(): List<PickTicket> {
        val pickTickets: MutableList<PickTicket> = mutableListOf()
        val requisitionLineMap: MutableMap<UUID, List<RequisitionLine>> = mutableMapOf()

        jdbcTemplate.queryForList("SELECT * FROM requisition_line ORDER BY createDate ASC").forEach { row ->
             val pickTicketId: UUID = UUID.fromString(row["pickTicketId"].toString())
             val requisitionLines: MutableList<RequisitionLine>

             if (requisitionLineMap.containsKey(pickTicketId)) {
                requisitionLines = requisitionLineMap[pickTicketId].orEmpty().toMutableList()
                requisitionLines.add(RequisitionLine(
                        UUID.fromString(row["id"].toString()),
                        row["qtyNeeded"].toString(),
                        UUID.fromString(row["parId"].toString())))
                 requisitionLineMap[pickTicketId] = requisitionLines.toList()
             } else {
                 requisitionLineMap[pickTicketId] = listOf(RequisitionLine(
                         UUID.fromString(row["id"].toString()),
                         row["qtyNeeded"].toString(),
                         UUID.fromString(row["parId"].toString())))
             }
        }

        jdbcTemplate.queryForList("SELECT * FROM pick_ticket ORDER BY createDate ASC").forEach { row ->
            val pickTicketId: UUID = UUID.fromString(row["id"].toString())

            pickTickets.add(PickTicket(
                    pickTicketId,
                    Location(UUID.fromString(row["supplyingLocationId"].toString()), row["supplyingLocationName"].toString(), true),
                    Location(UUID.fromString(row["requestingLocationId"].toString()), row["requestingLocationName"].toString(), false),
                    row["note"].toString(),
                    row["caseNumber"].toString(),
                    requisitionLineMap[pickTicketId].orEmpty()))
        }

        return pickTickets.toList()
    }

    fun count(): Int? {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM pick_ticket", Int::class.java)
    }

    fun findById(id: UUID): PickTicket? {
        val requisitionLines: MutableList<RequisitionLine> = mutableListOf()

        val namedParams: MutableMap<String, Any> = mutableMapOf()
        namedParams["pickTicketId"] = id.toString()

        namedParameterJdbcTemplate.queryForList("SELECT * FROM requisition_line WHERE pickTicketId = :pickTicketId", namedParams.toMap()).forEach { row ->
            requisitionLines.add(RequisitionLine(
                    UUID.fromString(row["id"].toString()),
                    row["qtyNeeded"].toString(),
                    UUID.fromString(row["parId"].toString())))
        }

        namedParams["id"] = id.toString()

        val pickTicket: PickTicket? = namedParameterJdbcTemplate.queryForObject("SELECT * FROM pick_ticket WHERE id = :id", namedParams
        ) { rs, rowNum ->
            val pickTicketId: UUID = UUID.fromString(rs.getString("id").toString())

            PickTicket(
                pickTicketId,
                Location(
                    UUID.fromString(rs.getString("supplyingLocationId").orEmpty()),
                    rs.getString("supplyingLocationName").orEmpty(),
                    true
                ),
                Location(
                    UUID.fromString(rs.getString("requestingLocationId").orEmpty()),
                    rs.getString("requestingLocationName").orEmpty(),
                    false
                ),
                rs.getString("note").orEmpty(),
                rs.getString("caseNumber").orEmpty(),
                requisitionLines
            )
        }

        return pickTicket
    }

    /*
     * Update Rules
     *   Can Update:
     *     Supplying Location ID/Name
     *     Note
     *     Case Number
     *     Quantity Needed
     */
    fun update(id: UUID, pickTicket: PickTicket) : PickTicket? {
        updatePickTicket(id, pickTicket)
        updateRequisitionLines(id, pickTicket)

        return findById(id)
    }


    fun updatePickTicket(id: UUID, pickTicket: PickTicket) {

        val namedParams: MutableMap<String, Any> = mutableMapOf()
        namedParams["pickTicketId"] = id.toString()
        namedParams["supplyingLocationId"] = pickTicket.supplyingLocation.id.toString()
        namedParams["supplyingLocationName"] = pickTicket.supplyingLocation.name
        namedParams["note"] = pickTicket.note
        namedParams["caseNumber"] = pickTicket.caseNumber.orEmpty()

        namedParameterJdbcTemplate.update("UPDATE pick_ticket SET " +
                "supplyingLocationId = :supplyingLocationId, " +
                "supplyingLocationName = :supplyingLocationName, " +
                "note = :note, " +
                "caseNumber = :caseNumber " +
                "WHERE id = :pickTicketId",
                namedParams
        )
    }

    fun updateRequisitionLines(id: UUID, pickTicket: PickTicket) {
        pickTicket.requisitionLines.forEach { line ->

            val namedParams: MutableMap<String, Any> = mutableMapOf()
            namedParams["pickTicketId"] = id.toString()
            namedParams["qtyNeeded"] = line.quantityNeeded
            namedParams["parId"] = line.parId

            namedParameterJdbcTemplate.update("UPDATE requisition_line SET " +
                    "qtyNeeded = :qtyNeeded " +
                    "WHERE pickTicketId = :pickTicketId " +
                    "AND parId = :parId",
                    namedParams
            )
        }
    }

    fun createTables() {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS pick_ticket" +
                "(" +
                    "id CHAR(36) NOT NULL, " +
                    "supplyingLocationId CHAR(36), " +
                    "supplyingLocationName VARCHAR(255), " +
                    "requestingLocationId CHAR(36), " +
                    "requestingLocationName VARCHAR(255), " +
                    "note VARCHAR(255), " +
                    "caseNumber VARCHAR(255), " +
                    "createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
                    "modifyDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " +
                    "PRIMARY KEY (id)" +
                ")")

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS requisition_line" +
                "(" +
                    "id CHAR(36) NOT NULL, " +
                    "pickTicketId CHAR(36), " +
                    "qtyNeeded TINYINT, " +
                    "parId CHAR(36), " +
                    "createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
                    "modifyDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " +
                    "PRIMARY KEY (id)" +
                ")")
    }
}
