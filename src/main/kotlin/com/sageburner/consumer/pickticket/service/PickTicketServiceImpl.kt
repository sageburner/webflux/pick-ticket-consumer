package com.sageburner.consumer.pickticket.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.sageburner.consumer.pickticket.model.PickTicket
import com.sageburner.consumer.pickticket.model.PickTicketEvent
import com.sageburner.consumer.pickticket.model.PickTicketEventType
import com.sageburner.consumer.pickticket.repository.jdbc.PickTicketJDBCRepository
import com.sageburner.consumer.pickticket.repository.kafka.PickTicketKafkaRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * PickTicketService
 *
 * Continually generate manual PickTicket requests and send them to configured Sink (REST endpoint, Message Queue, etc.)
 *
 */
@Service
class PickTicketServiceImpl : PickTicketService {


    private val log = LoggerFactory.getLogger(PickTicketServiceImpl::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    private lateinit var jdbcRepository: PickTicketJDBCRepository

    @Autowired
    private lateinit var kafkaRepository: PickTicketKafkaRepository

    private val mapper = jacksonObjectMapper()

    override fun createRx(pickTicket: PickTicket) : PickTicket {
        val newPickTicket = jdbcRepository.create(pickTicket)
        env.getProperty("kafka.topic.webflux")?.let {
            kafkaRepository.sendRx(
                it,
                PickTicketEvent(type = PickTicketEventType.CREATED, pickTicket = newPickTicket))
        }

        return newPickTicket
    }

    @Transactional
    override fun createRxTransactionally(pickTicket: PickTicket) : PickTicket {
        val newPickTicket = jdbcRepository.create(pickTicket)
        env.getProperty("kafka.topic.webflux")?.let {
            kafkaRepository.sendRxTransactionally(
                it,
                PickTicketEvent(type = PickTicketEventType.CREATED, pickTicket = newPickTicket))
        }

        return newPickTicket
    }

    override fun create(pickTicket: PickTicket) : PickTicket {
        val newPickTicket = jdbcRepository.create(pickTicket)
        env.getProperty("kafka.topic.webflux")?.let {
            kafkaRepository.send(
                it,
                PickTicketEvent(type = PickTicketEventType.CREATED, pickTicket = newPickTicket))
        }

        return newPickTicket
    }

    @Transactional
    override fun createTransactionally(pickTicket: PickTicket) : PickTicket {
        val newPickTicket = jdbcRepository.create(pickTicket)
        env.getProperty("kafka.topic.webflux")?.let {
            kafkaRepository.sendTransactionally(
                it,
                PickTicketEvent(type = PickTicketEventType.CREATED, pickTicket = newPickTicket))
        }
        return newPickTicket
    }

    override fun updateRx(id: UUID, pickTicket: PickTicket): PickTicket {
        val newPickTicket = jdbcRepository.update(id, pickTicket)

        env.getProperty("kafka.topic.webflux")?.let {
            kafkaRepository.sendRx(
                it,
                PickTicketEvent(type = PickTicketEventType.MODIFIED, pickTicket = newPickTicket!!))
        }

        return newPickTicket!!
    }

    @Transactional
    override fun updateRxTransactionally(id: UUID, pickTicket: PickTicket): PickTicket {
        val newPickTicket = jdbcRepository.update(id, pickTicket)
        env.getProperty("kafka.topic.webflux")?.let {
            kafkaRepository.sendRxTransactionally(
                it,
                PickTicketEvent(type = PickTicketEventType.MODIFIED, pickTicket = newPickTicket!!))
        }

        return newPickTicket!!
    }

    private fun jsonify(input: Any): String {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(input)
    }

    override fun listAll(): List<PickTicket> {
        return jdbcRepository.listAll()
    }

    override fun count(): Int {
        return jdbcRepository.count()!!
    }

    override fun findById(id: UUID): PickTicket {
        return jdbcRepository.findById(id)!!
    }
}
