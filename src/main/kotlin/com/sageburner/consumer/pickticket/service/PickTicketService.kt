package com.sageburner.consumer.pickticket.service

import com.sageburner.consumer.pickticket.model.PickTicket
import java.util.*


/**
 * PickTicketService
 *
 * Continually generate manual PickTicket requests and send them to configured Sink (REST endpoint, Message Queue, etc.)
 *
 */
interface PickTicketService {

    fun createRx(pickTicket: PickTicket) : PickTicket
    fun createRxTransactionally(pickTicket: PickTicket) : PickTicket
    fun create(pickTicket: PickTicket) : PickTicket
    fun createTransactionally(pickTicket: PickTicket) : PickTicket
    fun listAll() : List<PickTicket>
    fun count(): Int
    fun findById(id: UUID): PickTicket
    fun updateRx(id: UUID, pickTicket: PickTicket) : PickTicket
    fun updateRxTransactionally(id: UUID, pickTicket: PickTicket) : PickTicket
}
