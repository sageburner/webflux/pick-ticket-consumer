package com.sageburner.consumer.pickticket.controller

import com.sageburner.consumer.pickticket.model.PickTicket
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import java.util.*


interface PickTicketController {
    fun create(@RequestBody pickTicket: PickTicket): ResponseEntity<PickTicket>
    fun list(): ResponseEntity<List<PickTicket>>
    fun count(): ResponseEntity<Int>
    fun findById(@PathVariable id: UUID): ResponseEntity<PickTicket>
    fun update(@PathVariable id: UUID, @RequestBody pickTicket: PickTicket): ResponseEntity<PickTicket>

    //ToDo: Implement Spring Actuator for 'JMX'-type features
    //ToDo: Implement Swagger
//    fun health(): ResponseEntity<String>
}