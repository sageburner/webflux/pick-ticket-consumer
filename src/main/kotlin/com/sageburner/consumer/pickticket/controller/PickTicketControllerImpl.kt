package com.sageburner.consumer.pickticket.controller

import com.sageburner.consumer.pickticket.model.PickTicket
import com.sageburner.consumer.pickticket.service.PickTicketService
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController()
@RequestMapping(value = ["/pickticket"])
class PickTicketControllerImpl : PickTicketController {
    private val log = LoggerFactory.getLogger(PickTicketControllerImpl::class.java)

    @Autowired
    lateinit var pickTicketService: PickTicketService

    @RequestMapping(method = [RequestMethod.POST])
    @ApiOperation(value = "Create a Pick Ticket", notes = "Returns and stores a Pick Ticket based on the JSON request body", response = PickTicket::class)
    @ResponseBody
    override fun create(@RequestBody pickTicket: PickTicket): ResponseEntity<PickTicket> {
//        return ResponseEntity(pickTicketService.createRxTransactionally(pickTicket), HttpStatus.CREATED)
        return ResponseEntity(pickTicketService.createRx(pickTicket), HttpStatus.CREATED)
    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET], params = ["page", "size"])
    @ApiOperation(value = "List All Pick Tickets", notes = "Returns a list of all Pick Tickets", response = PickTicket::class, responseContainer="List")
    @ResponseBody
    override fun list(): ResponseEntity<List<PickTicket>> {
        return ResponseEntity(pickTicketService.listAll(), HttpStatus.OK)
    }

    @RequestMapping(value = ["/count"], method = [RequestMethod.GET])
    @ApiOperation(value = "Count All Pick Tickets", notes = "Returns a count of all Pick Tickets", response = Int::class)
    @ResponseBody
    override fun count(): ResponseEntity<Int> {
        return ResponseEntity(pickTicketService.count(), HttpStatus.OK)
    }

    @RequestMapping(value = ["/{id}"], method = [RequestMethod.GET])
    @ApiOperation(value = "Find Pick Ticket by ID", notes = "Returns the Pick Ticket referenced by the requested ID", response = PickTicket::class)
    @ResponseBody
    override fun findById(@PathVariable id: UUID): ResponseEntity<PickTicket> {
        return ResponseEntity(pickTicketService.findById(id), HttpStatus.OK)
    }

    @RequestMapping(value = ["/{id}"], method = [RequestMethod.PATCH])
    @ApiOperation(value = "Update a Pick Ticket", notes = "Updated and returns a Pick Ticket based on the JSON request body", response = PickTicket::class)
    override fun update(@PathVariable id: UUID, @RequestBody pickTicket: PickTicket): ResponseEntity<PickTicket> {
//        return ResponseEntity(pickTicketService.updateRxTransactionally(id, pickTicket), HttpStatus.OK)
        return ResponseEntity(pickTicketService.updateRx(id, pickTicket), HttpStatus.OK)
    }

}
