#!/usr/bin/env bash

docker run -d \
    --net=host \
    --name=zookeeper \
    -e ZOOKEEPER_CLIENT_PORT=32181 \
    confluentinc/cp-zookeeper:3.3.0

sleep 2

docker run -d \
    --net=host \
    --name=kafka01 \
    -e KAFKA_ZOOKEEPER_CONNECT=localhost:32181 \
    -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:29092 \
    -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=3 \
    confluentinc/cp-kafka:3.3.0

sleep 2

docker run -d \
    --net=host \
    --name=kafka02 \
    -e KAFKA_ZOOKEEPER_CONNECT=localhost:32181 \
    -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:29093 \
    -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=3 \
    confluentinc/cp-kafka:3.3.0

sleep 2

docker run -d \
    --net=host \
    --name=kafka03 \
    -e KAFKA_ZOOKEEPER_CONNECT=localhost:32181 \
    -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:29094 \
    -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=3 \
    confluentinc/cp-kafka:3.3.0

sleep 2

#docker run \
#  --net=host \
#  --rm confluentinc/cp-kafka:3.3.0 \
#  kafka-topics --create --topic webflux --partitions 1 --replication-factor 3 --if-not-exists --zookeeper localhost:32181
#
#docker run \
#  --net=host \
#  --rm \
#  confluentinc/cp-kafka:3.3.0 \
#  bash -c "echo 'Webflux Topic Created' | kafka-console-producer --request-required-acks 1 --broker-list localhost:29092 --topic webflux"