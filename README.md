# Pick Ticket Consumer

Consume REST requests, save the data to SQL database, publish message to Kafka

![Pick-Ticket Architecture](pick-ticket.png "Pick-Ticket Architecture")

## Build & Run

### Provision Pick Ticket Infrastructure

`docker-compose up -d`

### Run Locally (Boot Run)

`./gradlew bootRun`

### Run with Docker

Build the Docker image

`./gradlew buildDocker`

Build and Publish the Docker Image

`./gradlew buildDocker -Ppublish=true`

Run a Docker container

`docker run --rm --network=host --name=pick-ticket-consumer registry.gitlab.com/sageburner/webflux/pick-ticket-consumer:0.1.0`

## Tools

### MySQL
https://hub.docker.com/_/mysql

### Kafka (Confluent)
https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html